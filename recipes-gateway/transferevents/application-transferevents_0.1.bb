#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
#

SUMMARY = "Install Services"
SECTION = "Roadmaster"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "libxml2 libxslt busybox curl"

###INITSCRIPT_NAME = "transfereventsd"
###INITSCRIPT_PARAMS = "defaults 91"

inherit update-rc.d systemd

SYSTEMD_PACKAGES = "${PN}"
INITSCRIPT_PACKAGES = "${PN}"

SYSTEMD_SERVICE_${PN} = "transferevents.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

SRC_URI_append = " \
   file://transferevents.service \	
"

do_install_append() {
###	mkdir -p ${D}${sysconfdir}/init.d
###	install -D -m 0755 ${WORKDIR}/transferevents.service ${D}${sysconfdir}/init.d/transfereventsd
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/transferevents.service ${D}${systemd_system_unitdir} 
}
