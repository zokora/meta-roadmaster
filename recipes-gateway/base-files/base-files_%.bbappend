FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

dirs755_append += " \
	       /mnt/SSD \
"

inherit relative_symlinks

do_install_append () {
        sed -i '/root/s/0$/1/' ${D}${sysconfdir}/fstab
}

hostname="gateway"

