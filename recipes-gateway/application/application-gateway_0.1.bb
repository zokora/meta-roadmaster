#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
#

SUMMARY = "Install Services"
SECTION = "Roadmaster"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "sqlite3 "

###INITSCRIPT_NAME = "gatewayd"
###INITSCRIPT_PARAMS = "defaults 80"

inherit update-rc.d systemd

SYSTEMD_PACKAGES = "${PN}"
INITSCRIPT_PACKAGES = "${PN}"

SYSTEMD_SERVICE_${PN} = "gatewayd.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

SRCREV = "${AUTOREV}"
PV = "0.1+git${SRCPV}"

SRC_URI_append = " \
   file://20-config-eth0.network \
   file://21-config-eth1.network \
   file://22-config-usb0.network \
   file://transfer-events \
   file://image.version \	
   git://git@gitlab.etrps.de/rm/Eclipse-Application-Gateway.git;protocol=ssh;branch=master \
   file://90-journal.conf \
   file://crontab-gateway \
   file://init-network \
   file://gatewayd.service \
"
###   file://gatewayd \	
###


#   file://set-timeserver \
#

do_install_append() {
	mkdir -p ${D}${bindir}
	install  -D -m 0755 ${WORKDIR}/git/src/gateway ${D}${bindir}/gateway
	install  -D -m 0755 ${WORKDIR}/git/src/transferevents ${D}${bindir}/transferevents
	install  -D -m 0755 ${WORKDIR}/transfer-events 	${D}${bindir}/transfer-events
###	install  -D -m 0755 ${WORKDIR}/gatewayd 	${D}${bindir}

	mkdir -p ${D}${sysconfdir}/init.d
###	install -D -m 0755 ${WORKDIR}/gatewayd ${D}${sysconfdir}/init.d/gatewayd
	install -D -m 0755 ${WORKDIR}/image.version ${D}${sysconfdir}/image.version

	mkdir -p ${D}${sysconfdir}/systemd
	mkdir -p ${D}${sysconfdir}/systemd/network
	install -D -m 0755 ${WORKDIR}/20-config-eth0.network ${D}${sysconfdir}/systemd/network/20-config-eth0.network
	install -D -m 0755 ${WORKDIR}/21-config-eth1.network ${D}${sysconfdir}/systemd/network/21-config-eth1.network.sav
	install -D -m 0755 ${WORKDIR}/22-config-usb0.network ${D}${sysconfdir}/systemd/network/22-config-usb0.network

#	install  -D -m 0755 ${WORKDIR}/set-timeserver 	${D}${bindir}/set-timeserver
	install  -D -m 0755 ${WORKDIR}/init-network 	${D}${bindir}/init-network
	mkdir -p ${D}${sysconfdir}/cron.d
	install -D -m 0644 ${WORKDIR}/crontab-gateway ${D}${sysconfdir}/cron.d/crontab-gateway
	mkdir -p ${D}${sysconfdir}/systemd/journald.conf.d
	install -D -m 0755 ${WORKDIR}/90-journal.conf ${D}${sysconfdir}/systemd/journald.conf.d/90-journal.conf
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/gatewayd.service ${D}${systemd_system_unitdir} 
}
