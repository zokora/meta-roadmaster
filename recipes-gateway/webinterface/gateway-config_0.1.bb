#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
# test kds zum zweiten

SUMMARY = "Install Services"
SECTION = "Roadmaster"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"


FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

SRCREV = "${AUTOREV}"
PV = "0.1+git${SRCPV}"

SRC_URI_append = " \
   git://git@gitlab.etrps.de/rm/PHPRUNNER-Config.git;protocol=ssh;branch=master \
"

do_install_append() {
	mkdir -p ${D}${localstatedir}/www
	cp -R ${WORKDIR}/git/config ${D}${localstatedir}/www
	cd ${D}${localstatedir}/www/config
	chmod 0777 templates_c
}
