FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

SRC_URI_append = " \
  file://fec-enet-reset.patch \
  file://gateway.cfg  \
  file://0901-gateway-dtb.patch \
"

KERNEL_DEVICETREE = "imx6ull-txul-8013-mainboard.dtb"
