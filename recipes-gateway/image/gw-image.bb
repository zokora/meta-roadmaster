SUMMARY = "A console-only image with more full-featured Linux system functionality installed."


IMAGE_INSTALL = "\
    packagegroup-core-full-cmdline \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "

#    packagegroup-core-boot \
#

inherit core-image extrausers


LICENSE = "MIT"

EXTRA_IMAGE_FEATURES = " ssh-server-openssh"

EXTRA_USERS_PARAMS = "\
 useradd -P conf conf; \
 useradd -P gateway roadmaster; \
 usermod -P root root; \
"
IMAGE_INSTALL_append = " nano"
#IMAGE_INSTALL_append = " i2c-tools"
IMAGE_INSTALL_append = " curl libcurl"
IMAGE_INSTALL_append = " sqlite3 libsqlite3 libsqlite3-dev"
IMAGE_INSTALL_append = " php-cgi"
IMAGE_INSTALL_append = " hiawatha"
IMAGE_INSTALL_append = " gateway-config " 
IMAGE_INSTALL_append = " application-gateway "
IMAGE_INSTALL_append = " application-transferevents "
IMAGE_INSTALL_append = " python3 "


IMAGE_FSTYPES = "tar.bz2"



