FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

SRC_URI_append = " \
   file://sshd_config \
"


do_install_append() {
  mkdir -p ${D}${sysconfdir}/ssh
  install  -D -m 0644 ${WORKDIR}/sshd_config ${D}${sysconfdir}/ssh/sshd_config
}
