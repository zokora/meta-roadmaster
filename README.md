Ka-Ro Yocto BSP für Roadmaster

To get the BSP you need to have repo installed and use it as:

Install the repo utility:

mkdir ~/bin
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

Download the BSP source:

PATH=${PATH}:~/bin
mkdir karo-bsp
cd karo-bsp
repo init --no-clone-bundle -u https://github.com/karo-electronics/karo-bsp -b rocko
repo sync

At the end of the commands you have all metadata you need to start work with.
Building Linux Kernel and rootfs (for U-Boot see below!)

To start a simple Linux kernel and rootfs image build:

MACHINE=imx6q-tx6-nand source ./setup-environment build

Then copy following files from conf directory:
folder local_mnifests to .repo
local.conf and bblayers.conf to build/conf

Fetch this repo with "repo sync", then


bitbake gw-image

This will create the Image for Gateway
